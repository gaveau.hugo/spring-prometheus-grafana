package com.example.demo;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class ProductRoute {

    private Long generateRandomLong() {
        Random rd = new Random();
        return rd.nextLong();
    }

    @PostMapping(path = "/api/product", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Long addProduct(@RequestBody Product product) {
        return generateRandomLong();
    }
}
