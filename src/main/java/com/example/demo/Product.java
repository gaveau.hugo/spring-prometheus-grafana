package com.example.demo;

public class Product {
    private String name;
    private String userFirstame;
    private String userLastname;
    private String address;

    public Product(String name, String userFirstame, String userLastname, String address) {
        this.name = name;
        this.userFirstame = userFirstame;
        this.userLastname = userLastname;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserFirstame() {
        return userFirstame;
    }

    public void setUserFirstame(String userFirstame) {
        this.userFirstame = userFirstame;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
