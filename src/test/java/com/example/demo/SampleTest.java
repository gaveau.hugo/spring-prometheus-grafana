package com.example.demo;

import com.intuit.karate.junit5.Karate;

class SampleTest {

    @Karate.Test
    Karate testSample() {
        return Karate.run("api/product").relativeTo(getClass());
    }

}